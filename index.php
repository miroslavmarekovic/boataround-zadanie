<!DOCTYPE html>
<html>
	<head>
		<title>Zadanie - Boataround (Miroslav Marekovič)</title>
		
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		
		<link rel="stylesheet" href="style/bootstrap.min.css">
		<link rel="stylesheet" href="style/style.css">
	  
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="styler">
			<div class="center">
				<div class="col-md-6 col-md-offset-3 center-table">
					<table class="table super-table " id="pub-table">
						<thead>
							<tr>
								<th>Názov</th>
								<th>Kategória</th>
								<th>Adresa</th>
								<th>Vzdialenosť</th>
							</tr>
						</thead>
						<tbody>
							<tr></tr>
							<!-- OBSAH TABUĽKY -->
						</tbody>
					</table>
				</div>
			</div>
		</div>


	</body>
	<script src="js/table.js"></script>	
</html>